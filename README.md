# Projet : Météo Cahors

### Objectif :

Manipuler les données d'une API avec des requêtes Ajax pour afficher des informations dynamiquement sur un site web.  

### Consignes :

> Regarder le wireframe pour la mise en page !

- Récuperer votre clef d'api pour openweathermap (si besoin !)
- Avec la documentation de **[openweathermap](https://www.openweathermap.org/current)** afficher la carte de Cahors.
- Dans une jolie **card** afficher le nom de la ville, les coordonnées GPS, la vitesse du vent, le taux d'humidité, la pression, la temperature/min/max (convertie en Celsius).  

### Contraintes : 

- Utiliser Gulp,
- Utiliser SASS,
- Si vous le souhaitez (optionnel) : utilisation de jQuery et/ou d'un framework CSS.  

### Bonus :

- Créer un formulaire de recherche permettant à l'utilisateur de rechercher la météo par ville,
- Avec l'api de **[leaflet](http://leafletjs.com/reference-1.2.0.html)** et la longitude et latitude récupérées grâce à openweathermap, ajouter un markeur sur la map.  

### Durée : 
2 / 3 jours.
