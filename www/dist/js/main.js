let mymap;
document.getElementById('btnSubmit').addEventListener('click',choixVille);

function choixVille(){

	  console.log("bonjour");

    let choix = document.getElementById('choixVille').value;
    
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${choix}&appid=53235902cd3754af316f31751978cdf3&units=metric`) 
    .then(res => res.json())
    .then(data => {
      weatherData(data)
      map(data)
      weatherConditions(data);
    })

};

function weatherConditions(data) {
  let ciel = data.weather[0].main;
  document.getElementById('card').className = ciel.toLowerCase();
};

function weatherData(data){ 

  let city = document.getElementById('city');
  let gps = document.getElementById('gps');
  let tempActual = document.getElementById('tempActual');
  let wind = document.getElementById('wind');
  let humidity = document.getElementById('humidity');
  let pressure = document.getElementById('pressure');


  city.innerHTML = "Vous consultez la météo de " + data.name;
  gps.innerHTML = "Les coordonées gps sont, latitude " + data.coord.lat + " et longitude " + data.coord.lon;
  tempActual.innerHTML = "La température actuelle est de " + data.main.temp + "°C";
  wind.innerHTML = "La vitesse du vent est de " + data.wind.speed + "Km/h";
  humidity.innerHTML = "L'humidité est de " + data.main.humidity + "%";
  pressure.innerHTML = "La pression atmosphérique est de " + data.main.pressure + " hPa";


};


function map(data) {

  if (mymap != undefined) {
      mymap.remove();
  }

  mymap = L.map('mapid').setView([data.coord.lat, data.coord.lon], 13);

    let plan = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'pk.eyJ1Ijoiam9uYXRoYW4tdCIsImEiOiJjanJhbTE1aHYwMjM5NDNtbDc1aTJmazV0In0.soOYBX8OrT7CuDteP2I4RQ'
    });
    plan.addTo(mymap);
};


