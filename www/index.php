<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <title>La météo !</title>
  <link rel="stylesheet" type="text/css" href="dist/css/main.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
  integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
  crossorigin=""/>
</head>
<body>
  <header>
  </header>
  <main>
  		<div id="card"  class="weatherConditions">  
	      <H1>Météo</H1>
	      <div>
	      	<input id="choixVille" type="text" placeholder="Votre ville">
	      	<button id="btnSubmit">Voir</button>
	      </div>
	      <div id="mapid" class="map"></div>
	      <ul class="info">
	        <li id="city" class="villeAfficher"></li>
	        <div><hr></div>
	        <li id="gps"></li>
	        <div><hr></div>
	        <li id="tempActual"></li>
	        <div><hr></div>
	        <li id="wind"></li>
	        <div><hr></div>
	        <li id="humidity"></li>
	        <div><hr></div>
	        <li id="pressure"></li>
	       </ul>
	    </div>  
  </main>
 	<footer></footer>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
     	integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
     	crossorigin=""></script>
    <script type="text/javascript" src="dist/js/main.js"></script>
	</body>
</html>
